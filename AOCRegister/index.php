
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" language="javascript"  src="jquery-1.9.1.js"></script>
				
        <title></title>
        <script type="text/javascript">
            
            

            function validate()
            {
                var college_roll_no=$('#college_roll_no').val();
				var university_roll_no=$('#university_roll_no').val();
                var name=$('#name').val();
				var f_name=$('#f_name').val();
				var m_name=$('#m_name').val();
				var dob=$('#dob').val();
                var class_sub=$('#class_sub').val();
                var mob_no = $('#mob_no').val();
				var class_course=$('#class_course').val();
				var Sem_III=$('#Sem_III').val();
				var Sem_IV=$('#Sem_IV').val();
			
								
                if(college_roll_no==="")
                {
                     alert("College Roll No. cann't be blank.");   
                     return false;
                }else if(university_roll_no ==="")
                {
                    alert("University Roll No. cann't be blank.");
                     return false;
                }
                else if(name ==="")
                {
                    alert("name cann't be blank.");
                     return false;
                }else if(f_name ==="")
                {
                    alert("father's name cann't be blank.");
                     return false;
                }else if(m_name ==="")
                {
                    alert("mother's name cann't be blank.");
                     return false;
                }else if(dob ==="")
                {
                    alert("date of birth cann't be blank.");
                     return false;
                }else if(mob_no ==="")
				{
					alert("mobile no cann't be blank.");
					return false;
				}
				else if(isNaN(mob_no))  
				{
					alert("Please enter the right mobile no.");
                     return false;
				}else if(mob_no.length !==10)  
				{
					alert("Please enter the right mobile no.");
                     return false;
				}
				else if(class_sub ==="select")
                {
                    alert("12th class subject cann't be blank.");
                     return false;
                }else if(class_course ==="select")
                {
                    alert("Last Semester FC’s Group cann't be blank.");
                     return false;
                
                }else if(Sem_III==="")
                {
                    alert("Please Add Subject For First Prefence.");
                     return false;
                }else if(Sem_IV==="")
                {
                    alert("Please Add Subject For Second Prefence.");
                     return false;
                }else{
                    /*document.regForm.method = "post";
                    document.regForm.action = "Registration.php";
                    document.regForm.submit();*/
                         return true;
                }
                
                
            }

       function showSub(elem)
       {
        if(elem.value == 0)
          document.getElementById("hidden").style.display="block";
        else
          document.getElementById("hidden").style.display="none";
       }     
			      
            </script>

<style type="text/css">
.main{ width:100%; float:left;}
.container{width: 1000px;
/* float: left; */
margin: 0 auto;
}

.form{
/* float: left; */


width: 738px;
/* float: left; */

padding-left: 113px;
margin-left: 86px; border-left:4px solid #ccc; border-right:4px solid #ccc; border-bottom:4px solid #ccc; }


.imp{ padding: 7px 6px 7px 6px;
width: 281px;
border-radius: 4px;
margin: 0px 6px 13px 0px;
border: none;
border: 1px solid #308721;}
.text{width: 307px;
padding: 15px 0px 71px 49px;
border-radius: 4px;
margin-bottom: 11px; border: 1px solid #308721;}
.inner-text{font-family: arial;
font-size: 14px;
color: #308721;}

.sub{padding: 10px 27px 10px 27px;
border: 1px solid;
border-radius: 4px;
margin-left: 239px;
margin-top: 8px;
margin-bottom: 29px; cursor:pointer;}

.sub:hover{ background: #308721; color:#FFFFFF;}
.heading{width: 851px;
text-align: center;
border-left:4px solid #ccc; border-right:4px solid #ccc; border-top:4px solid #ccc;
margin-left: 86px;
padding: 20px 0px 39px 0px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:15px; font-weight:bold;color: #308721;}

.subheading{width: 851px;
text-align: center;
border-left:4px solid #ccc; border-right:4px solid #ccc; border-top:4px solid #ccc;
margin-left: 86px;
padding: 20px 0px 39px 0px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;color: #308522;}


.inp {
padding: 7px 6px 7px 6px;
width: 281px;
border-radius: 4px;
margin: 0px 6px 13px 0px;
border: none;
border: 1px solid #308721;
}
			
			
			</style>
    </head>
    <body onLoad="subjectValue();">
    <div class="main">
     <div class="container">
     <div class="heading"> ADD ON COURSE<br><i><font color="Blue">Course Option Form</font></i></div>
     
     <form action="Registration.php" id="regForm" name="regForm" method="POST" onSubmit="return(validate());">
     <div class="form">   
         <table>
             <th ><span></span></th>
             <!--<tr><td class="inner-text"> College Roll Number<font color="red">*</font></td><td><input type="text" id="college_roll_no" name="college_roll_no" class="imp" /></td></tr>
			 <tr><td class="inner-text"> University Roll Number<font color="red">*</font></td><td><input type="text" id="university_roll_no" name="university_roll_no" class="imp" /></td></tr>-->
             <tr><td  class="inner-text">Name<font color="red">*</font></td><td><input type="text" id="name" name="name" class="imp" /></td></tr>
			 <tr><td  class="inner-text">Father's Name<font color="red">*</font></td><td><input type="text" id="f_name" name="f_name" class="imp" /></td></tr>
			 <tr><td  class="inner-text"> Mother's Name:<font color="red">*</font></td><td><input type="text" id="m_name" name="m_name" class="imp" /></td></tr>
			 <tr><td  class="inner-text">Date of Birth:<font color="red">*</font></td><td><input type="text" id="dob" name="dob"   class="imp" />
			 <tr><td  class="inner-text"> E-Mail Address:<font color="red">*</font></td><td><input type="text" id="e_mail" name="e_mail" class="imp" /></td></tr>

			 </td></tr>
             <tr><td  class="inner-text">Mobile Number.:<font color="red">*</font></td><td><input type="text" id="mob_no" name="mob_no"  class="imp"/></td></tr>
            
             <tr><td  class="inner-text">Address:</td><td><textarea id="address" name="address"  class="text"></textarea></td></tr>
			 
			 <tr><td  class="inner-text">12th class subjects stream:<font color="red">*</font></td><td><select id="class_sub" name="class_sub" onChange="" class="inp">
                         <option value="select">--Select--</option>                     
						 <option value="Art">Art</option>                     
						 <option value="Commerce">Commerce</option>                     
						 <option value="Science">Science</option> 
						 <option value="Other">Other</option> 
                         
                     </select></td></tr>
			
			       <tr><td class ="inner-text">Graduation<font color="red">*</font></td>
            <td><select id="grad_course" name="grad_course" onChange="" class="inp">
                         <option value="select">--Select--</option>                     
						 <option value="BA">B.A.</option> 
                         <option value="BCOM">B.Com.</option>
                         <option value="BSCH">B.Sc.</option>
                         <option value="BE">B.E./B.Tech.</option> 
                         <option value="BCA">BCA</option>
                         <option value="BBA">BBA.</option> 
                     </select></td></tr>
                <tr><td  class="inner-text">12 percentage<font color="red">*</font></td><td><input type="text" id="pc" name="pc" class="imp" /></td></tr>
                     
			 
			    <!--   <tr><td class ="inner-text">Year<font color="red">*</font></td>
            <td><select id="class_sub" name="class_sub" onChange="showSub(this)" class="inp">
                         <option value="select">--Select--</option>                     
						 <option value="0">II Year(2016-17)</option>                     
						 <option value="1">III Year(2016-17)</option>                             
                     </select></td></tr>
			-->
			
<tr><td colspan="2">
			<fieldset>
    <legend>Options for ADD ON COURSE </legend>
<div id=hidden style="display:block">

<table style="width: 100%;padding: 100">	
             <tr>
                <td>
                    <input type="checkbox" name="dotnet" value="dotnet">Microsoft .Net Framework</input>
                </td>
                <td>
                    <input type="checkbox" name="SEO" value="SEO">SEO/SMO</input>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" name="tally" value="tally">Tally</input>
                </td>
                <td>
                    <input type="checkbox" name="paralegal" value="paralegal">Paralegal</input>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" name="jyotish" value="jyotish">Jyotish and Vaastu Shastra</input>
                </td>
            </tr>


             	<!--
                <td>
             		<select id="Sem_III" name="Sem_III"  onchange="" class="inp">
                         <option value="select">--Select--</option>
                          <option value="Bio-Chemistry">.NET</option>
                          <option value="Bio-Chemistry">SEO/SME</option>
                          <option value="Bio-Chemistry">Tally</option>
                          <option value="Bio-Chemistry">Paralegal</option>
                          <option value="Bio-Chemistry">Jyotish Shastra</option>      
                    </select>
                 </td>
              </tr>-->
              <!--
			<tr>
				<td  class="inner-text">Choose one by one priority & Add Subject:
				</td>
				<td>
					<label><input type="radio" id="f_p_radio_btn" name="radio_btn" value="f_p_radio" checked="true"/>First Preference
					</label>
					<label><input type="radio" id="s_p_radio_btn" name="radio_btn" value="s_p_radio"/>Second Preference
					</label>
					<label><input type="radio" id="t_p_radio_btn" name="radio_btn" value="t_p_radio"/>Third Prefence</label>
				</td>
			</tr>
            <tr>
            	<td  class="inner-text">Subjects Option for DC-II:<font color="red">*</font>
            	</td>
             	<td>
             		<select id="course_name" name="course_name" onChange="otherVaue();" class="inp">
                         <option value="select">--Select--</option>                     
                    </select>
                </td>
            </tr>
              <tr><td class="inner-text"><div id="other" style="display:none;"> Other Subjects:<font color="red">*</font></div></td><td><div id="other1" style="display:none;"><select id="other_subject" name="other_subject" class="inp">
                         <option value="select">--Select--</option>                     
                         
                     </select></div><input type="button" id="add_sub" name="add_sub" value="Add Subject" onClick="addSubject()"/></td></tr>
					 <tr><td colspan="2"><table><tr><td>First Preference Subject:<input type="text" id="f_p_sub_txt" name="f_p_sub_txt" readonly="true"/></td><td>Second Preference Subject:<input type="text" id="s_p_sub_txt" name="s_p_sub_txt" readonly="true"/></td></tr><tr><td>Third Preference Subject:<input type="text" id="t_p_sub_txt" name="t_p_sub_txt" readonly="true"/></td><td></td></tr></table></td></tr>
					 </table>
             </fieldset>       
           </td></tr>-->
		   <tr><td colspan="2"><input type="submit" id="submit" name="submit" value="Submit" class="sub"></td></tr>
        
        </table></div>
        </div>

     </form>
     </div>
     </div>
    </body>
</html>
