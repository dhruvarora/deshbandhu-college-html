-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 18, 2016 at 12:17 PM
-- Server version: 5.7.13
-- PHP Version: 5.5.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Add_On_Courses`
--

-- --------------------------------------------------------

--
-- Table structure for table `Student_Info`
--

CREATE TABLE `Student_Info` (
  `Name` varchar(30) NOT NULL,
  `Father` varchar(30) NOT NULL,
  `Mother` varchar(30) NOT NULL,
  `Mobile` varchar(20) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Address` varchar(400) DEFAULT NULL,
  `Inter_subject` varchar(20) NOT NULL,
  `Graduation` varchar(20) NOT NULL,
  `DOB` varchar(20) NOT NULL,
  `dotnet` varchar(10) NOT NULL,
  `SEO` varchar(10) NOT NULL,
  `tally` varchar(10) NOT NULL,
  `paralegal` varchar(10) NOT NULL,
  `jyotish` varchar(10) NOT NULL,
  `Token` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Student_Info`
--
ALTER TABLE `Student_Info`
  ADD PRIMARY KEY (`Token`),
  ADD UNIQUE KEY `Unique` (`Token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Student_Info`
--
ALTER TABLE `Student_Info`
  MODIFY `Token` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
